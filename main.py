import json
import flask
from pprint import pprint
from pprint import pformat
import os
# from pdfdocument.document import PDFDocument


def split_json(initial_file, part_1_suffix='1', part_2_suffix='2', split_threshold=0):
    initial_split = initial_file.split('.')
    part_1 = open(f'{initial_split[0]}_{part_1_suffix}.{initial_split[1]}', 'a')  # open file for append
    part_2 = open(f'{initial_split[0]}_{part_2_suffix}.{initial_split[1]}', 'a')  # open file for append
    count = 0
    with open(initial_file) as fp:
        for line in fp:  # we iterate through lines as on every line there is a JSON string
            count += 1
            try:
                json_view = json.loads(line.strip())  # here we parsing string to dictionary
                if count < split_threshold:
                    part_1.write(line)
                else:
                    part_2.write(line)
            except json.JSONDecodeError as e:
                pass

    part_1.close()  # after we've done writing, we need to close file
    part_2.close()  # after we've done writing, we need to close file


def filter_json(file_path, event_type='alert', name_suffix=''):
    event_type_filename = f'{event_type}{name_suffix}.json'

    # check if the file is already exists
    if os.path.exists(event_type_filename):
        answer = input(
            f'{event_type_filename} already exists, do you want to [o]verwrite, or [a]ppend, [e]xit? ')
        if answer == 'o':
            os.remove(event_type_filename)
        elif answer == 'e':
            print('Exiting')
            return
        elif answer != 'a':
            print('Unknown command, exiting')
            return

    write_file = open(event_type_filename, 'a')  # open file for append

    event_types = {}  # dict to count all encountered types
    count = 0  # variable to count lines
    errors = 0
    errors_list = []
    with open(file_path) as fp:
        print('Number of lines have been read (millions): ')
        for line in fp:  # we iterate through lines as on every line there is a JSON string
            if count % 1000000 == 0:
                print(count // 1000000, end='...')  # print progress every 1000000 lines
            count += 1
            try:
                json_view = json.loads(line.strip())  # here we parsing string to dictionary

                if json_view['event_type'] == event_type:  # if there is a match by event_type, append to filter file
                    write_file.write(line)

                if json_view['event_type'] not in event_types:
                    event_types[json_view['event_type']] = 1
                else:
                    event_types[json_view['event_type']] += 1
            except json.JSONDecodeError as e:
                errors += 1  # count for encountered errors
                errors_list.append(
                    f'Error: [{e}] on line {count}: {line}')  # some lines has errors and cannot be parsed as JSON

    write_file.close()  # after we've done writing, we need to close file
    print()
    print(f"Event_types: {event_types}, Total_lines: {count}")
    print(f'[Errors: {errors}]')
    print("\n".join(errors_list))


def find_flows():
    pdf = PDFDocument('./alerts_flow.pdf')
    pdf.init_report()

    flows = [960235938718942, 911088628771334, 932402404622003, 1905970559560689, 383793348336987, 68940033885779,
             1477375822896496, 1802766848152286, 749022410220104, 1003043960998707, 1670320819902184, 1602361556372476,
             1236718249665396, 680691784541635, 86414346358534, 1021536105331243, 8535858385714, 1443967620220678,
             1851946571666722, 1595953647526007, 799419757048925, 1219736002881416, 1157207719569079, 1541246523460205,
             1460636606734998, 7451618957287, 1435128823410940, 383841139005657, 1691579235452965, 2100221756746178,
             1891200740719814, 141079010663520, 1893314005024497, 1545838268430941, 523816447389317, 1880493544471992,
             104597628577769, 1647483039437401, 758261608333135, 1175380245396640, 887600268777208, 976123969685003,
             1773239843081602, 1433950313750709, 771181032919406,
             2157364561114548, 933432039073364, 900390869677395, 141126548111951, 573342027230292, 1255992864730072,
             1495196781258013, 1353273883671598, 173850097304601, 1512947888647963, 1990956277215182, 334314388297083,
             1863223970341547, 936623439322368, 988708511417756, 1783779974963038, 1758448392749200,
             2021072760618184, 252216250915277, 811287849846654, 1442424706642655, 2036526058512349, 388435438891148,
             2070383287707672, 661195937504095, 1527735655536680, 884225031669929, 1496605793259843, 2054689549400408,
             100599765915074, 1901397958907049, 1248103517671371, 558752689892781, 2239974689240602, 110293725914246,
             833789566064022, 366810710678081, 1989973343132739, 300848617872922, 634498883822975, 972998145546787,
             833050933191213, 1696330795269852, 979217289058333, 996384280387228, 556274693544008, 670258907087331,
             715794151011326, 2111005691879817, 1165691981852220, 2067862748984452]
    count = 0
    with open('flow.json') as fp:
        for line in fp:
            if count == len(flows):
                pdf.generate()
                return
            json_view = json.loads(line.strip())
            if json_view['flow_id'] in flows:
                count += 1
                pdf.p(pformat(json_view, indent=4))
                pdf.p('\n')
                pprint(json_view)
    pdf.generate()


def find_correlated_alerts(file_path='alert.json', save=''):
    categories = {}
    groups_dict = {}
    count = 0
    with open(file_path) as fp:
        for line in fp:
            try:
                count += 1
                json_view = json.loads(line.strip())
                key = f"{json_view['alert']['signature']}_{json_view['src_ip']}_{json_view['dest_ip']}"
                if key in groups_dict:
                    groups_dict[key]['Count'] += 1
                    groups_dict[key]['flow_ids'].append(json_view['flow_id'])

                else:
                    groups_dict[key] = {"key": key, "Signature": json_view['alert']['signature'],
                                        "Source IP": json_view['src_ip'], "Destination IP": json_view['dest_ip'],
                                        "Count": 1, "flow_ids": [json_view['flow_id']]}
                    # Basicaly we stack every type_src_dst we found into dict

            except json.JSONDecodeError as e:
                print(e, line)

    grouped = [groups_dict[k] for k in groups_dict if
               groups_dict[k]['Count'] > 1]  # Now we filter for those, who has more than 1 correlation

    if save:
        with open(save, 'w') as outfile:
            json.dump({k: groups_dict[k] for k in groups_dict if groups_dict[k]['Count'] > 1}, outfile)

    count_by_category = {el['Signature'] for el in grouped}  # template to count by types of signatues
    count_by_category_by_group = {k: sum([1 for e in grouped if e['Signature'] == k]) for k in
                                  count_by_category}  # count the number of groups by categoies
    count_by_category = {k: sum([e['Count'] for e in grouped if e['Signature'] == k]) for k in
                         count_by_category}  # count the number of records that fall into each category

    print(len(grouped), '/', count)
    # print('Count by category by group: ', count_by_category_by_group, sep='\n')  # separate print by new line
    # print('Count by category: ', count_by_category, sep='\n')  # separate print by new line

    pprint(grouped[:10])


def generate_pdf():
    json_files = [file for file in os.listdir() if file[-4:] == 'json']
    pdf = PDFDocument('./my.pdf')
    pdf.init_report()

    for json_file in json_files:
        with open(json_file) as fp:
            line = fp.readline()
            json_view = json.loads(line.strip())
        pdf.h1(json_file)
        pdf.p(pformat(json_view, indent=4))
        pdf.p('\n')

    pdf.generate()


def find_repeated(saved='correlated_1.json'):
    threats = {}
    with open(saved, 'r') as read_file:
        groups_dict = json.load(read_file)
    with open('alert_2.json') as fp:
        for line in fp:
            try:
                json_view = json.loads(line.strip())
                key = f"{json_view['alert']['signature']}_{json_view['src_ip']}_{json_view['dest_ip']}"
                if key in groups_dict:
                    if key in threats:
                        threats[key]['Count'] += 1
                    else:
                        threats[key] = {"key": key, "Signature": json_view['alert']['signature'],
                                        "Source IP": json_view['src_ip'], "Destination IP": json_view['dest_ip'],
                                        "Count": 1}
            except json.JSONDecodeError as e:
                print(e, line)
    pprint([{"old" :groups_dict[k], "new": threats[k]} for k in threats][:10])


def parse_logs(file_name='conn.log.labeled'):
    with open(file_name) as fp:
        for i, line in enumerate(fp):
            if i > 20:
                return
            if i >= 6:
                parsed = line.split('\x09')
                parsed = parsed[:-1] + [el for el in parsed[-1].split(' ') if len(el)>0]
                print(parsed, len(parsed))
            print(line, i)



if __name__ == '__main__':
    # split_json(initial_file='alert.json', split_threshold=112041 // 2)
    # filter_json(file_path='eve_1.json', name_suffix='_1')  # filter huge .json and make smaller .json to work with
    find_correlated_alerts(file_path='alert_1.json', save='correlated_1.json')
    # find_repeated()
    # split_json()
    # generate_pdf()
    # filter_json('eve.json')  # filter huge .json and make smaller .json to work with
    # find_correlated_alerts()  # find correlated alerts and print them
    # find_correlated_alerts(file_path='collerated_last.json', )  # find correlated alerts and print them

    # parse_logs()
